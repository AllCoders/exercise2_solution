﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Excercise2.Entities;

namespace Excercise2.Repositories
{
    public interface IComicRepository
    {
        Task<List<Comic>> GetAllComicsAsync();
        Task<Comic> GetComicByIdAsync(Guid id);
    }
}
