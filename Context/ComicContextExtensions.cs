﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Excercise2.Entities;
using Newtonsoft.Json;

namespace Excercise2.Context
{
    public static class ComicContextExtensions
    {
        public static void EnsureSeed(this ComicContext context, string rootPath)
        {
            if (!context.Comics.Any())
            {
                var comics = JsonConvert.DeserializeObject<List<Comic>>(File.ReadAllText(Path.Combine(rootPath, "comicsimport.json")));

                context.Comics.AddRange(comics);
                context.SaveChanges();
            }
        }
    }
}
