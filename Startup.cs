﻿using Excercise2.Context;
using Excercise2.Repositories;
using Excercise2.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Excercise2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<IComicRepository, ComicRepository>();

            var databaseSetting = new DatabaseSetting();
            Configuration.GetSection("Database").Bind(databaseSetting);

            // Registering the Context so it can be resolved
            services.AddDbContext<ComicContext>(o => o.UseSqlServer(databaseSetting.ConnectionString));

            // Migrating the database automatically with the latest changes
            // Because we work code first, in this case it will create the database for you
            services.BuildServiceProvider().GetService<ComicContext>().Database.Migrate();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ComicContext comicContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionHandler>();

            comicContext.EnsureSeed(env.WebRootPath);

            app.UseMvc();

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Our Comic service api is running!");
            });
        }
    }
}
